let nextButton = document.getElementById("next-btn");
let detailsContainer = document.getElementById("details-container");
let planContainer = document.getElementById("plan-container");
let addOnContainer = document.getElementById("add-on-container");
let summaryContainer = document.getElementById("summary-container");

//page1 to page2
let nameElement = document.getElementById("name");
let emailElement = document.getElementById("email");
let phoneElement = document.getElementById("phone");
let nameError = document.getElementById("name-error");
let emailError = document.getElementById("email-error");
let phoneError = document.getElementById("phone-error");
let bar2 = document.getElementById("bar-2");
let bar1 = document.getElementById("bar-1");
let backButton2 = document.getElementById("back-btn-2");

nextButton.addEventListener("click", function () {
  if (nameElement.value === "") {
    nameError.textContent = "This field is required";
    nameElement.style.borderColor = "hsl(354, 84%, 57%)";
  } else {
    nameError.textContent = "";
    nameElement.style.borderColor = "";
  }
  if (emailElement.value === "") {
    emailError.textContent = "This field is required";
    emailElement.style.borderColor = "hsl(354, 84%, 57%)";
  } else {
    emailError.textContent = "";
    emailElement.style.borderColor = "";
  }
  if (phoneElement.value === "") {
    phoneError.textContent = "This field is required";
    phoneElement.style.borderColor = "hsl(354, 84%, 57%)";
  } else {
    phoneError.textContent = "";
    phoneElement.style.borderColor = "";
  }
  if ((nameElement.value && emailElement.value && phoneElement.value) !== "") {
    detailsContainer.classList.add("none");
    planContainer.classList.remove("none");
    bar1.classList.remove("active-sidebar");
    bar2.classList.add("active-sidebar");
  }
});

backButton2.addEventListener("click", function () {
  detailsContainer.classList.remove("none");
  planContainer.classList.add("none");
  bar1.classList.add("active-sidebar");
  bar2.classList.remove("active-sidebar");
});

//page 2- page-3
let nextButton2 = document.getElementById("next-btn-2");
let bar3 = document.getElementById("bar-3");
let switchToggle = document.getElementById("switch-toggle");
let arcade = document.getElementById("arcade");
let advanced = document.getElementById("advanced");
let pro = document.getElementById("pro");
let togglePlanArcade = document.getElementById("toggle-plan-arcade");
let togglePlanAdvanced = document.getElementById("toggle-plan-advanced");
let togglePlanPro = document.getElementById("toggle-plan-pro");
let offerPlan = document.querySelectorAll("#offer-plan");
let backButton3 = document.getElementById("back-btn-3");
let cost1 = document.getElementById("cost-1");
let cost2 = document.getElementById("cost-2");
let cost3 = document.getElementById("cost-3");
let plan = document.getElementById("plan");
let monthYear = document.getElementById("month-year");
let monthYearPlan = document.getElementById("yearPlan-montPlan");
let totalMonthYear = document.getElementById("total-monthYear");
let planCostElement = document.getElementById("plan-cost");

let monthly = document.getElementById("monthly");
let yearly = document.getElementById("yearly");

let selectedPlan = "Arcade";
let totalCost = 0;

function updatePlanCost() {
  if (selectedPlan === "Arcade ") {
    planCostElement.textContent = togglePlanArcade.textContent;
  } else if (selectedPlan === "Advanced ") {
    planCostElement.textContent = togglePlanAdvanced.textContent;
  } else if (selectedPlan === "Pro ") {
    planCostElement.textContent = togglePlanPro.textContent;
  }
}

switchToggle.addEventListener("change", function () {
  if (this.checked) {
    yearly.classList.add("new-blue");
    monthly.classList.add("new-cement");
    togglePlanArcade.textContent = "$90/yr";
    togglePlanAdvanced.textContent = "$120/yr";
    togglePlanPro.textContent = "$150/yr";
    cost1.textContent = "+$10/yr";
    cost2.textContent = "+$20/yr";
    cost3.textContent = "+$20/yr";
    monthYear.textContent = "(Yearly)";
    monthYearPlan.innerText = "Total (per year)";
    totalMonthYear.innerText = "yr";
    offerPlan.forEach(function (element) {
      element.textContent = "2 months free";
    });
  } else {
    yearly.classList.remove("new-blue");
    monthly.classList.remove("new-cement");
    togglePlanArcade.textContent = "$9/mo";
    togglePlanAdvanced.textContent = "$12/mo";
    togglePlanPro.textContent = "$15/mo";
    cost1.textContent = "+$1/mo";
    cost2.textContent = "+$2/mo";
    cost3.textContent = "+$2/mo";
    monthYear.textContent = "(Monthly)";
    monthYearPlan.innerText = "Total (per month)";
    totalMonthYear.innerText = "mo";
    offerPlan.forEach(function (element) {
      element.textContent = "";
    });
  }
  updatePlanCost();
});

arcade.addEventListener("click", function () {
  if (!arcade.classList.contains("plan-design")) {
    arcade.classList.add("plan-design");
    advanced.classList.remove("plan-design");
    pro.classList.remove("plan-design");
    selectedPlan = "Arcade ";
    plan.textContent = selectedPlan;
    updatePlanCost();
  } else {
    arcade.classList.remove("plan-design");
    selectedPlan = "";
  }
});

advanced.addEventListener("click", function () {
  if (!advanced.classList.contains("plan-design")) {
    advanced.classList.add("plan-design");
    arcade.classList.remove("plan-design");
    pro.classList.remove("plan-design");
    selectedPlan = "Advanced ";
    plan.textContent = selectedPlan;
    updatePlanCost();
  } else {
    advanced.classList.remove("plan-design");
    selectedPlan = "";
  }
});

pro.addEventListener("click", function () {
  if (!pro.classList.contains("plan-design")) {
    pro.classList.add("plan-design");
    arcade.classList.remove("plan-design");
    advanced.classList.remove("plan-design");
    selectedPlan = "Pro ";
    plan.textContent = selectedPlan;
    updatePlanCost();
  } else {
    pro.classList.remove("plan-design");
    selectedPlan = "";
  }
});

nextButton2.addEventListener("click", function () {
  if (selectedPlan !== "") {
    addOnContainer.classList.remove("none");
    planContainer.classList.add("none");
    bar3.classList.add("active-sidebar");
    bar2.classList.remove("active-sidebar");
    updatePlanCost();
    totalCost += parseInt(
      planCostElement.textContent.slice(
        1,
        planCostElement.textContent.indexOf("/")
      )
    );
  }
});

backButton3.addEventListener("click", function () {
  planContainer.classList.remove("none");
  addOnContainer.classList.add("none");
  bar3.classList.remove("active-sidebar");
  bar2.classList.add("active-sidebar");
  totalCost = 0;
});

//page3 to page4

let nextButton3 = document.getElementById("next-btn-3");
let backButton4 = document.getElementById("back-btn-4");
let planChangeBtn = document.getElementById("plan-change-btn");
let bar4 = document.getElementById("bar-4");
let checkbox1 = document.getElementById("checkbox-1");
let checkbox2 = document.getElementById("checkbox-2");
let checkbox3 = document.getElementById("checkbox-3");
let onlineService = document.getElementById("online-service");
let largerStorage = document.getElementById("larger-storage");
let customProfile = document.getElementById("custom-profile");
let addOnService = document.getElementById("add-on-service");
let totalCostElement = document.getElementById("total-costFinal");

let addService = document.getElementById("add-service");
let addStorage = document.getElementById("add-storage");
let addProfile = document.getElementById("add-profile");


let serviceArray = [];
let eachserciceCost = 0;



checkbox1.addEventListener("change", function() {
  if (checkbox1.checked) {
    addService.classList.add("add-ons-checked");
  } else {
    addService.classList.remove("add-ons-checked");
  }
});


checkbox2.addEventListener("change", function() {
  if (checkbox2.checked) {
    addStorage.classList.add("add-ons-checked");
  } else {
    addStorage.classList.remove("add-ons-checked");
  }
});

checkbox3.addEventListener("change", function() {
  if (checkbox3.checked) {
    addProfile.classList.add("add-ons-checked");
  } else {
    addProfile.classList.remove("add-ons-checked");
  }
});




nextButton3.addEventListener("click", function () {
  let getcost1 = cost1.textContent;
  if (checkbox1.checked) {
    let existingService = serviceArray.find(
      (service) => service.serviceName === "Online Service"
    );

    if (existingService) {
      existingService.serviceCost = getcost1;
    } else {
      serviceArray.push({
        serviceName: "Online Service",
        serviceCost: getcost1,
      });
    }
  } else {
    let existingServiceIndex = serviceArray.findIndex(
      (service) => service.serviceName === "Online Service"
    );

    if (existingServiceIndex !== -1) {
      serviceArray.splice(existingServiceIndex, 1);
    }
  }

  let getcost2 = cost2.textContent;
  if (checkbox2.checked) {
    let existingService = serviceArray.find(
      (service) => service.serviceName === "Larger Storage"
    );

    if (existingService) {
      existingService.serviceCost = getcost2;
    } else {
      serviceArray.push({
        serviceName: "Larger Storage",
        serviceCost: getcost2,
      });
    }
  } else {
    let existingServiceIndex = serviceArray.findIndex(
      (service) => service.serviceName === "Larger Storage"
    );

    if (existingServiceIndex !== -1) {
      serviceArray.splice(existingServiceIndex, 1);
    }
  }

  let getcost3 = cost3.textContent;
  if (checkbox3.checked) {
    let existingService = serviceArray.find(
      (service) => service.serviceName === "Customizable Profile"
    );

    if (existingService) {
      existingService.serviceCost = getcost3;
    } else {
      serviceArray.push({
        serviceName: "Customizable Profile",
        serviceCost: getcost3,
      });
    }
  } else {
    let existingServiceIndex = serviceArray.findIndex(
      (service) => service.serviceName === "Customizable Profile"
    );

    if (existingServiceIndex !== -1) {
      serviceArray.splice(existingServiceIndex, 1);
    }
  }
  console.log(serviceArray);
  serviceArray.forEach((each) => {
    let serviceElementContainer = document.createElement("li");
    let nameElement = document.createElement("p");
    let costElement = document.createElement("p");
    nameElement.classList.add("service-name");
    costElement.classList.add("service-cost");
    nameElement.textContent = each.serviceName;
    costElement.textContent = each.serviceCost;
    serviceElementContainer.appendChild(nameElement);
    serviceElementContainer.appendChild(costElement);
    serviceElementContainer.classList.add("service-design");
    addOnService.appendChild(serviceElementContainer);
    eachserciceCost += parseInt(
      each.serviceCost.slice(2, each.serviceCost.indexOf("/"))
    );
  });
  summaryContainer.classList.remove("none");
  addOnContainer.classList.add("none");
  bar4.classList.add("active-sidebar");
  bar3.classList.remove("active-sidebar");
  console.log(eachserciceCost);
  console.log(eachserciceCost + totalCost);
  totalCostElement.textContent = eachserciceCost + totalCost;
});

backButton4.addEventListener("click", function () {
  summaryContainer.classList.add("none");
  addOnContainer.classList.remove("none");
  bar4.classList.remove("active-sidebar");
  bar3.classList.add("active-sidebar");
  addOnService.innerHTML = "";
  eachserciceCost = 0;
});

planChangeBtn.addEventListener("click", function () {
  summaryContainer.classList.add("none");
  planContainer.classList.remove("none");
  bar4.classList.remove("active-sidebar");
  bar2.classList.add("active-sidebar");
  addOnService.innerHTML = "";
  eachserciceCost = 0;
  totalCost = 0;
});

// finishing page
let finishContainer = document.getElementById("finish-container");
let confirmBtn = document.getElementById("confirm-btn");
confirmBtn.addEventListener("click", function () {
  summaryContainer.classList.add("none");
  finishContainer.classList.remove("none");
});
